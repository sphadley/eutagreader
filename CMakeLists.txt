cmake_minimum_required (VERSION 2.8)
project (EuTagReader)

SET(CMAKE_CXX_FLAGS "-std=c++0x")

if(UNIX)
   include_directories(${CMAKE_SOURCE_DIR}/deps/linux/include/)
elseif(WIN32)
   include_directories(${CMAKE_SOURCE_DIR}/deps/windows/include/)
endif()

include_directories(${CMAKE_SOURCE_DIR})

file(GLOB EuTagReader_SRC
    "*.hpp"
    "main.cpp"
)

add_executable(EuTagReader ${EuTagReader_SRC})
if(UNIX)
   link_directories(${CMAKE_SOURCE_DIR}/deps/linux/lib/)
elseif(WIN32)
   link_directories(${CMAKE_SOURCE_DIR}/deps/windows/lib/)
endif()

if(UNIX)
	target_link_libraries( EuTagReader opencv_highgui opencv_imgproc opencv_imgcodecs opencv_core opencv_features2d zxing )
elseif(WIN32)
    target_link_libraries( EuTagReader opencv_highgui310 opencv_imgproc310 opencv_imgcodecs310 opencv_core310 opencv_features2d310 libzxing )
endif()

configure_file(img/0.png 0.png COPYONLY)
configure_file(img/1.png 1.png COPYONLY)
configure_file(img/2.png 2.png COPYONLY)
configure_file(img/3.png 3.png COPYONLY)
configure_file(img/4.png 4.png COPYONLY)
configure_file(img/5.png 5.png COPYONLY)
configure_file(img/6.png 6.png COPYONLY)
configure_file(img/7.png 7.png COPYONLY)
configure_file(img/8.png 8.png COPYONLY)
configure_file(img/9.png 9.png COPYONLY)
configure_file(img/filled.png filled.png COPYONLY)
