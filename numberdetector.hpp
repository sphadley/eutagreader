#ifndef NUMBERDETECTOR_H
#define NUMBERDECTECTOR_H

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d.hpp>

using namespace std;
class NumberDetector 
{
    private:
    cv::Mat _imageTemplate;

    public :
    string Text = "";
    NumberDetector(string path, string text)
    {
        auto cPath = path.c_str();
        if ((_imageTemplate = cv::imread((char*)cPath, 1)).data == 0)
        {
            std::cout << "bad path image template: " + path << std::endl;
        }

        cv::cvtColor(_imageTemplate, _imageTemplate,CV_RGB2GRAY);
        Text = text;
    }

    bool DetectImage(cv::Mat &src)
    {
        cv::Mat result;
        cv::matchTemplate(src, _imageTemplate, result, CV_TM_CCORR_NORMED);
        double minVal, maxVal;
        cv::Point minPt, maxPt;
        cv::minMaxLoc(result, &minVal, &maxVal, &minPt, &maxPt);
        return maxVal > 0.98;
    }
        
};


#endif // NUMBERDETECTOR_H
