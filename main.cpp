#include <iostream>
#include <fstream>
#include <zxing/ZXing.h>
#include <zxing/LuminanceSource.h>
#include <zxing/common/GreyscaleLuminanceSource.h>
#include <zxing/datamatrix/DataMatrixReader.h>
#include <zxing/common/HybridBinarizer.h>
#include <numberdetector.hpp>
//#include <ctype.h>

using namespace std;
using namespace zxing;


vector<cv::Point>  getDMGuides(cv::Mat src, cv::Mat element)
{
    //isolate either horizontal or vertical lines
    cv::Mat result;
    cv::cvtColor(src,result,CV_RGB2GRAY);
    for(int i =0; i <8; i++)
        cv::dilate(result,result,element);

    for(int i =0; i <8; i++)
        cv::erode(result,result,element);

     vector<vector<cv::Point> > contours;
     vector<cv::Point> guide;
     vector<cv::Vec4i> hierarchy;
     //find the location of the guide
     findContours( result, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );

     for(auto c :contours)
     {
         cv::Point2f center;
         float radius;
         cv::minEnclosingCircle(c,center,radius);
         //filterout everything on the edges or that is too long
         //(ie. the edge of the tag
         if(center.x > 10 && center.y > 10 && radius > 40 && radius < 170)
         {
             guide = c;
             break;
         }
     }

    return guide;
}

string readBarCode(cv::Mat src, cv::Rect &barcodeRect)
{
    //first lets do some morphology  operations to find the guide
    //bars of the data matrix barcode, this will tell us where to
    //have zebracrossing look, since apparently its not smart enough
    //to deal with the whole image
    cv::Mat element1 = getStructuringElement(cv::MORPH_RECT, cv::Size(1, 10));
    cv::Mat element2 = getStructuringElement(cv::MORPH_RECT, cv::Size(10, 1));
    vector<cv::Point> horizGuide = getDMGuides(src,element2);
    vector<cv::Point> vertGuide = getDMGuides(src,element1);
    //put all of the points together
    for(auto c : vertGuide)
        horizGuide.push_back(c);
    //find the bounding box that contains both guides with
    //a little padding for good measure
    if(horizGuide.size() == 0)
        return "";
    cv::Rect rect =cv::boundingRect(horizGuide);
    rect.height +=12;
    rect.width += 7;
    rect.x = rect.x -5;
    rect.y = rect.y - 12;
    //seperate out the submatrix that contains only the barcode
    cv::Mat dmMat;
    src(rect).copyTo(dmMat);
    cv::cvtColor(dmMat,dmMat,CV_RGB2GRAY);
    //feed zebracrossing data matrix reader
    ArrayRef<char> data = ArrayRef<char>((char*)dmMat.data,  dmMat.rows * dmMat.step);
    Ref<GreyscaleLuminanceSource> lumisource(new GreyscaleLuminanceSource(data, dmMat.step, dmMat.rows,0,0, dmMat.cols, dmMat.rows));
    Ref<HybridBinarizer> binarizer(new HybridBinarizer(lumisource));
    Ref<BinaryBitmap> binmap( new BinaryBitmap(binarizer));
    datamatrix::DataMatrixReader reader;
    DecodeHints hints(true);
    Ref<Result> result = reader.decode(binmap,hints);
    barcodeRect = rect;

    return result->getText()->getText();
}

string readDots(cv::Mat &src, string execDir)
{
    //First find the submatrix that contains the filled dot
    cv::Mat dotTemplate = cv::imread(execDir + "/filled.png",1);
    cv::Mat result;
    cv::cvtColor(src,src,CV_RGB2GRAY);
    cv::cvtColor(dotTemplate,dotTemplate,CV_RGB2GRAY);
    cv::matchTemplate(src,dotTemplate, result,CV_TM_CCORR_NORMED);
    double minVal, maxVal;
    cv::Point minPt, maxPt;
    cv::minMaxLoc(result, &minVal, &maxVal, &minPt, &maxPt);
    cv::Mat dotMat;
    src(cv::Rect(0, minPt.y -10, src.cols,20)).copyTo(dotMat);
    //Use template matching to determine the value of the trait
    //based on the number label next to it.
    vector<NumberDetector> detectors = vector<NumberDetector>();
    for(int i =0; i <10; i++)
       detectors.push_back( NumberDetector(execDir +"/" +to_string(i) +".png", to_string(i)));

    //Then run the submatrix through the number detectors
    //and return the matching string
    for(auto detector : detectors)
    {
        if(detector.DetectImage(dotMat))
            return detector.Text;
    }

    return "";
}

string removeLastOf(string path, string symbol)
{
    size_t found = path.find_last_of(symbol);
    return path.substr(0,found);
}

bool isValidId(string value)
{
	for (char c : value)
	{
		if (!isdigit(c))
			return false;
	}
	return true;
}

int main(int argc, char* argv[])
{
    //parse arguments
    //I know I should be doing boost filesystem here, but in the essense
    //of time I am going to avoid it this time around. It would take
    //as long to compile and bundle as the rest of this took
    //to write.
    string execPath = string(argv[0]);
    string execDir =removeLastOf(execPath, "/\\");
    if(argc ==1)
    {
        cout << "Please enter space separated list of input file names" <<endl;
        return 1;
    }
    vector<string> inputs =  vector<string>();
    for(int i =1; i < argc; i++)
    {
        inputs.push_back(string(argv[i]));
    }

    for(auto input: inputs)
    {
        string inputName = removeLastOf(input, ".");
        //processimage
        cv::Mat src = cv::imread( input ,1);
        if(src.data ==0)
            cout << "bad input file" + input << endl;
        cv::Mat leftSection;
        cv::Mat rightSection;
        cv::Rect barcodeRect;
        string eu =readBarCode(src,barcodeRect);
		if (!isValidId(eu))
		{
			cout << "EUID is invalid for " + input << endl;
			continue;
		}
        if(eu == "")
        {
            cout << "could not read barcode on input " + input  << endl;
            continue;
        }
        src(cv::Rect(0,0, src.cols/2, barcodeRect.y)).copyTo(leftSection);
        src(cv::Rect(src.cols/2,0, src.cols/2, barcodeRect.y)).copyTo(rightSection);
        string plot = readDots(leftSection, execDir);
        string polcnt = readDots(rightSection, execDir);
        if(plot == "" || polcnt == "")
        {
            cout << "could not read dots" << endl;
            continue;
        }

        //output to csv
        ofstream output = ofstream(inputName+".csv", ofstream::trunc);
        output << "EUID,Plot,POLCNT" <<endl;
        output << eu + "," + plot + "," +polcnt;
    }
}

